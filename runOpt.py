"""
AWAKE Run 2 Electron Line Model
R. Ramjiawan
Jun 2020
Track beam through line and extract beam parameters at merge-point
"""

# from scipy.stats.stats import _threshold_mgc_map
# from torch import true_divide
import OptEnv as opt_env
import errorOptEnv as errorEnv
import plot_save_output as plot_self
from scipy.optimize import minimize
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec
import pickle
import sys
import warnings
warnings.filterwarnings("error", category=RuntimeWarning)
import logging


# Initial values for quadrupoles (q), sextupoles (s), octupoles (o) and distances (a)
# nominal




q0=1.4305269096909254
q1=4.389518465570255
q2=7.897355074613861
q3=4.201377505310179
q4=-5.254599391447313
q5=-3.465904348351117
q6 = 0
q7 = 0



# # enter the values here over which to optimise, otherwise hard-code them into MAD-X file
x = {
    0: {'name': 'quad0', 'strength': q0, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    1: {'name': 'quad1', 'strength': q1, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    2: {'name': 'quad2', 'strength': q2, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    3: {'name': 'quad3', 'strength': q3, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    4: {'name': 'quad4', 'strength': q4, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    5: {'name': 'quad5', 'strength': q5, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    6: {'name': 'quad6', 'strength': q6, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
    7: {'name': 'quad7', 'strength': q7, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},

}

# Specify parameters for optimisation
# filename = 'general_tt43_python_foil.madx'
filename = 'general_tt43_python_no_foil.madx'
# solver = 'Powell'
solver = 'Nelder-Mead'
n_iter = 2000
n_particles = 1000  # Used to generate distribution to track
foil_w = 0.001e-6
init_dist = []
thin = True
file = 'distr/Ellipse_150MeV_nominal.tfs'
test = True # If test=True the nominal optics is run
run_error = False

a14=0



if test:
    x = {
        0: {'name': 'quad0', 'strength': q0, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        1: {'name': 'quad1', 'strength': q1, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        2: {'name': 'quad2', 'strength': q2, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        3: {'name': 'quad3', 'strength': q3, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        4: {'name': 'quad4', 'strength': q4, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        5: {'name': 'quad5', 'strength': q5, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        6: {'name': 'quad6', 'strength': q6, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},
        7: {'name': 'quad7', 'strength': q7, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,1)},

    }




# Initialise environment

env = opt_env.kOptEnv(solver, n_particles, n_iter, init_dist, foil_w, x, thin=thin, file=filename)
# Initialise input distribution
init_dist = np.zeros((n_particles, 6))
covx = np.array([[5e-6**2,0], [0, 1e-3**2]])
xxp = np.random.multivariate_normal(np.array([0,0]), covx, size=n_particles)
covy = np.array([[5e-6**2,0], [0, (2e-3)**2]])
yyp = np.random.multivariate_normal(np.array([0,0]), covy, size=n_particles)
covz = np.array([[2e-6**2,0], [0, 2e-4**2]])
zzp = np.random.multivariate_normal(np.array([0,0]), covz, size=n_particles)
init_dist[:,0] = xxp[:,0]
init_dist[:,3] = xxp[:,1]
init_dist[:,1] = yyp[:,0]
init_dist[:,4] = yyp[:,1]
init_dist[:,2] = zzp[:,0]
init_dist[:,5] = zzp[:,1]
env.init_dist = init_dist


# n_p_range = [1000,5000,10000,20000,30000,40000,50000, 100000]
# frac = []
# for n_p in n_p_range:
#     # env = opt_env.kOptEnv(solver, n_particles, n_iter, init_dist, foil_w, x, thin=thin, file=filename)
#     env = opt_env.kOptEnv(solver, n_p, n_iter, init_dist, foil_w, x, thin=thin, file=filename)
#     # Initialise input distribution
#     var = []
#     f = open(file, 'r')  # initialize empty array
#     for line in f:
#         var.append(
#             line.strip().split())
#     f.close()
#     init_dist = np.array(var)[0:n_p, 0:6].astype(np.float)
#     env.init_dist = init_dist
#     del var

#     # if test == 'On':
#     a = env.simple_track(env.norm_data([y['strength'] for y in x.values()]))
#     loss = a[3]
#     # env.step(env.norm_data([y['strength'] for y in x.values()]))
#     # out_plot = plot_self.Plot(env.madx, env.x_best, x, init_dist, foil_w, env.output_all, env.x_all)
#     # out_plot.MapTable()

#     # out_plot.twiss()
#     # print('Size of env.x_best: '+str(np.shape(env.x_best)))
#     # out_plot.emittance()
#     # out_plot.plotmat()
#     # out_plot.phase()
#     # sys.exit()

#     # sys.exit()
#     out_part_frac = 100-(loss/n_p)*100
#     threshold_np_3s = 99.95
#     threshold_np_35s = 99.73
#     frac.append(out_part_frac)
#     print(f'Transmitted particles = {out_part_frac} %')
# plt.plot(n_p_range, frac, label = 'Transmitted particles', marker = 'x')
# plt.hlines(threshold_np_3s, 0, 110000, label = 'particles in 3.5$\sigma$', color = 'r', linestyle = '--')
# plt.hlines(threshold_np_35s, 0, 110000, label = 'particles in 3$\sigma$', color = 'g', linestyle = '--')
# plt.xlim(0,110000)
# plt.xlabel('N particles')
# plt.ylabel('Transmitted particles [%]')
# plt.legend()
# plt.show()

if test:
    # env = opt_env.kOptEnv(solver, n_particles, n_iter, init_dist, foil_w, x, thin=thin, file=filename)


    # if test == 'On'
    
    env.step(env.norm_data([y['strength'] for y in x.values()]))
    out_plot = plot_self.Plot(env.madx, env.x_best, x, init_dist, foil_w, env.output_all, env.x_all)
    # out_plot.MapTable()
    # out_plot.envelopes()

    # out_plot.twiss()
    # print('Size of env.x_best: '+str(np.shape(env.x_best)))
    # out_plot.emittance()
    # out_plot.plotmat()
    # out_plot.phase()

    sys.exit()
# sys.exit()
# Optimise
if solver != "pyMOO" and solver!='ZOOpt' and test!=True:

    bnds = [y['limits'] for y in x.values()]
    # bounds = bnds
    solvers = ['Nelder-Mead', 'Powell', 'CG', 'BFGS', 'Newton-CG']


    solution = minimize(env.step, x0 = env.norm_data([y['strength'] for y in x.values()]), method=solver, options={'maxfev':n_iter,'adaptive': True})
    # print(solution.x)
    out_plot = plot_self.Plot(env.madx, env.x_best, x, init_dist, foil_w, env.output_all, env.x_all)
    # out_plot.twiss()
    # out_plot.plotmat()
    # out_plot.plot1()
    env.step(env.norm_data(env.unnorm_data(solution.x)))
    # plot.error()
    # plot.plotmat()
    sys.exit()
elif solver == 'pyMOO':
    from pymoo.core.problem import Problem
    from pymoo.core.problem import ElementwiseProblem
    from pymoo.algorithms.moo.nsga2 import NSGA2
    from pymoo.algorithms.soo.nonconvex.ga import GA
    from pymoo.factory import get_sampling, get_crossover, get_mutation
    from pymoo.optimize import minimize
    from pymoo.factory import get_termination
    from pymoo.core.problem import starmap_parallelized_eval
    from multiprocessing.pool import ThreadPool
    from pymoo.visualization.scatter import Scatter
    from pymoo.algorithms.soo.nonconvex.pso import PSO


    x_0 = env.norm_data([y['strength'] for y in x.values()])
    norm_vect = env.norm_data([y['norm'] for y in x.values()])
    n_obj = 1

    # Build boundaries for optimization fro x vector
    up_lim = []
    low_lim = []
    for y in x.values():
        low_lim.append(y['limits'][0])
        up_lim.append(y['limits'][1])

    n_threads = 4
    pool = ThreadPool(n_threads)


    class MatchingProblem(opt_env.kOptEnv, Problem):
        def __init__(self,
                     norm_vect,
                     x_0,
                     n_var=len(x_0),
                     n_obj=n_obj,
                     n_constr=0,
                     xl=low_lim,
                     xu=up_lim):
            opt_env.kOptEnv.__init__(self, solver, n_particles, n_iter, init_dist, foil_w, x, thin=thin, file=filename)
            Problem.__init__(self,
                             n_var=len(x_0),
                             n_obj=n_obj,
                             n_constr=n_constr,
                             # xl=-np.ones(np.shape(norm_vect)),
                             # xu=np.ones(np.shape(norm_vect)))
                             xl = low_lim,
                             xu = up_lim)
        def _evaluate(self, x_n, out, *args, **kwargs):
            # f = []
            params = [[x_n[k]] for k in range(x_n.shape[0])]


            # calculate the function values in a parallelized manner and wait until done
            F = pool.starmap(self.step_MO, params)
            out["F"] = np.array(F)
    # the number of threads to be used


    # initialize the pool


    # define the problem by passing the starmap interface of the thread pool

    problem = MatchingProblem(
        norm_vect = norm_vect,
        n_var = len(x_0),
        n_obj = n_obj,
        n_constr = 0,
        x_0 = x_0,
        # xl=-np.ones(np.shape(norm_vect)),
        # xu=np.ones(np.shape(norm_vect)))
        xl = low_lim,
        xu = up_lim
    )

    # problem.evaluate(np.vstack([problem.x_0, problem.x_0, -np.ones_like(problem.x_0)]))

    algorithm = GA(
        pop_size=100,
        n_offsprings=100,
        sampling=get_sampling("real_lhs"),
        crossover=get_crossover("real_sbx", prob=0.9, eta=15),
        mutation=get_mutation("real_pm", eta=30),
        eliminate_duplicates=True
    )

    # termination = MultiObjectiveDefaultTermination(
    #     x_tol=1e-8,
    #     cv_tol=1e-6,
    #     f_tol=1e-7,
    #     nth_gen=5,
    #     n_last=30,
    #     n_max_gen=50000,
    #     n_max_evals=200000
    # )
    termination = get_termination("n_iter", 500)




    res = minimize(problem,
                   PSO(),
                   termination,
                   algorithm,
                   seed=1,
                   ngen = 100
                   )

    ps = problem.pareto_set(use_cache=False, flatten=False)
    pf = problem.pareto_front(use_cache=False, flatten=False)
    np.savetxt('x_best.txt', problem.x_best)

    # Plotting functions
    import plot_save_output as plot
    name = [y['name'] for y in x.values()]
    plot = plot.Plot(env.madx, problem.x_best, x, init_dist, foil_w, problem.output_all, problem.x_all)
    for j in range(len(problem.x_best)):
        env.madx.input(name[j] + "=" + str(problem.x_best[j]) + ";")
        print(name[j] + "=" + str(problem.x_best[j]) + ";")
        env.madx.use(sequence='TT43', range='#s/#e')
    plot.plotmat_twiss()
    plot.twiss()
    # plot.plot1(problem.output_all)
    plot2 = Scatter()
    plot2.add(res.F, color="red")
    plot2.show()

    fig = plt.figure(figsize=[8, 7], constrained_layout=True)
    gs = fig.add_gridspec(1, 1)
    ax1 = plt.subplot(gs[:])
    # ax1.plot(np.zeros(shape=  (algorithm.pop_size)), problem.output_all[0:algorithm.pop_size, 0], 'o')
    generations = int(len(problem.output_all[algorithm.pop_size:, -1]) / algorithm.n_offsprings)
    mean = np.zeros(shape=(generations))
    max = np.zeros(shape=(generations))
    min = np.zeros(shape=(generations))
    iterations = np.zeros(shape=(generations))
    for i in range(generations):
        v = problem.output_all[algorithm.pop_size + i * algorithm.n_offsprings:algorithm.pop_size + (
                i + 1) * algorithm.n_offsprings, -2]
        mean[i] = (np.mean(v[v < 10 ** 21]))
        min[i] = (np.min(v[v < 10 ** 21]))
        max[i] = (np.max(v[v < 10 ** 21]))
        iterations[i] = i +1
    ax1.fill_between(iterations, min, max, alpha=0.1, color="tab:blue")
    ax1.plot(iterations, mean, '-o', label="mean(objective)", color="tab:blue")
    ax1.plot(iterations, min, '-', linewidth=0.4, color="tab:blue")
    ax1.plot(iterations, max, '-', linewidth=0.4, color="tab:blue")
    ax1.set_yscale('log')
    ax1.set_xlabel("Generation")
    ax1.set_ylabel("Objective function")
    plt.show()
    # ax2 = ax1.twinx()
    # mean = np.zeros(shape=(generations))
    # max = np.zeros(shape=(generations))
    # min = np.zeros(shape=(generations))
    # iterations = np.zeros(shape=(generations))
    # for i in range(generations):
    #     v = problem.output_all[algorithm.pop_size + i * algorithm.n_offsprings:algorithm.pop_size + (
    #             i + 1) * algorithm.n_offsprings, 4]
    #     mean[i] = np.mean(v[v < 1 * 10 ** 6])
    #     min[i] = np.min(v[v < 1 * 10 ** 6])
    #     max[i] = np.max(v[v < 1 * 10 ** 6])
    #     iterations[i] = i +1
    # ax2.fill_between(iterations, min, max, alpha=0.05, color='tab:orange')
    # ax2.plot(iterations, mean, '-', color='tab:orange', label="mean($\sigma_x$)")
    # ax2.plot(iterations, min, '-', linewidth=0.2, color="tab:orange")
    # ax2.plot(iterations, max, '-', linewidth=0.2, color="tab:orange")
    # mean = np.zeros(shape=(generations))
    # max = np.zeros(shape=(generations))
    # min = np.zeros(shape=(generations))
    # iterations = np.zeros(shape=(generations))
    # for i in range(generations):
    #     v = problem.output_all[algorithm.pop_size + i * algorithm.n_offsprings:algorithm.pop_size + (
    #             i + 1) * algorithm.n_offsprings, 3]
    #     mean[i] = np.mean(v[v < 1 * 10 ** 6])
    #     min[i] = np.min(v[v < 1 * 10 ** 6])
    #     max[i] = np.max(v[v < 1 * 10 ** 6])
    #     iterations[i] = i +1
    # ax2.fill_between(iterations, min, max, alpha=0.05, color='tab:green')
    # ax2.plot(iterations, mean, '-', color='tab:green', label="mean($\sigma_y$)")
    # ax2.plot(iterations, min, '-', linewidth=0.2, color="tab:green")
    # ax2.plot(iterations, max, '-', linewidth=0.2, color="tab:green")
    # ax2.set_ylabel("Beam size [$\mu$m]")
    # fig.legend(loc="center")
    # ax2.set_yscale('log')
    ax1.set_yscale('log')
    ax1.set_xlabel("Generation")
    ax1.set_ylabel("Objective function")
    # ax1.set_xlim([1, iterations[-1]])
    # ax2.set_ylim([0, 1000])




elif solver == 'ZOOpt':
    from zoopt import Dimension, ValueType, Dimension2, Objective, Parameter, Opt, ExpOpt

    dim_size = 100  # dimension size
    dim = Dimension(dim_size, [[-1, 1]] * dim_size,
                    [True] * dim_size)  # dim = Dimension2([(ValueType.CONTINUOUS, [-1, 1], 1e-6)]*dim_size)
    obj = Objective(env.step, dim)
    # perform optimization
    solution = Opt.min(obj, Parameter(budget=100 * dim_size))
    # print the solution
    print(solution.get_x(), solution.get_value())
    # parallel optimization for time-consuming tasks
    solution = Opt.min(obj, Parameter(budget=100 * dim_size, parallel=True, server_num=3))
    plt.plot(obj.get_history_bestsofar())
    plt.show()


# ERROR STUDIES






#
#
# bsx_all = np.zeros(nseeds)
# bsy_all = np.zeros(nseeds)
# ox_all = np.zeros(nseeds)
# oy_all = np.zeros(nseeds)
# tsx_all = np.zeros(nseeds)
# tsy_all = np.zeros(nseeds)
# power_jit = 0*10e-6
# mom_jit = 0*1e-3
# input_jit = 0*10e-6
# proton_jit = 0*56
# for i in range(nseeds):
#     print(i)
#     scale = np.random.randint(1, 10000)
#     # errorEnv.addError(scale)
#     # errorEnv.OneToOne()
#     # errorEnv.addPowerConv(power_jit, i)
#     errorEnv.calcOffsets(8)
#     # print("before" + str(bsx) +" "+str(bsy)+" "+str(osx)+" "+str( osy))
#     # errorEnv.OneToOne()
#     # errorEnv.track()
#     bsx_all[i] = bsx
#     bsy_all[i] = bsy
#     ox_all[i] = osx
#     oy_all[i] = osy

#     print("after" + str(bsx) + " " + str(bsy) + " " + str(osx) + " " + str(osy))
#     tsx_all[i] = (proton_jit*np.random.normal() - osx)
#     tsy_all[i] = (proton_jit*np.random.normal() - osy)
#     print(bsx, bsy, osx, osy, tsx_all[i], tsy_all[i])
#     print((bsx < 6.9) & (bsy < 6.9), (tsx_all[i] < 13) & (tsy_all[i]< 13))
# print(sum((bsx_all < 6.9) & (bsy_all < 6.9)), sum((-13<tsx_all ) & (-13<tsy_all )&(tsx_all < 13) & (tsy_all < 13)), sum((-13<tsx_all ) & (-13<tsy_all )&(tsx_all < 13) & (tsy_all < 13)
#                                                                                 & (bsx_all < 6.9) & (bsy_all < 6.9)))
# print(np.mean(bsx_all), np.mean(bsy_all), np.mean(tsy_all), np.mean(tsy_all))
#
# nseeds = 2
# # errorEnv = errorEnv.Error(env.x_best, x, init_dist, n_particles)
# # Model steering algorithms
# errorEnv = errorEnv.Error(env.x_best, x, init_dist, n_particles)
# errorEnv.track()
# print("Adding the errors")
# errorEnv.addError(0)
# errorEnv.track()
# #
# # # errorEnv.OptScanMO()
# # print("doing svd")
# # errorEnv.svd_plot(10, 1)  # Takes n_iter, gain as input
# # print("doing dfs")
# # errorEnv.dfs_plot(10, 2, 3, 0.95)    # Number of seeds, dfs weight, number of iterations of steering, gain
#
# fig = plt.figure()
# gs = matplotlib.gridspec.GridSpec(7, 1)
# ax = fig.add_subplot(gs[0:3])
# # ax1 = fig.add_subplot(gs[4:7])
# ax.plot([], '-', color=[0, 0.324219, 0.628906], label="Uncorrected")
# ax.plot([], '-', color="darkorange", label="Corrected")
# # ax1.plot([], '-', color=[0, 0.324219, 0.628906], label="Uncorrected")
# # ax1.plot([], '-', color="darkorange", label="Corrected")
# ax.legend()
# # ax1.set_xlabel("s [m]")
# # ax1.set_ylabel("Offset $y$ [mm]")
# # # ax1.set_ylim([-5, 5])
# ax.set_xlabel("s [m]")
# ax.set_ylabel("Offset $x$ [mm]")
# ax.set_ylim([-5, 5])
# ax.set_title("Quad shunt")
# # ax1.legend()
# #
# # # # # Quad shunt, dfs
# Quad Shunt optics
# meas_optics = True
# if meas_optics:
#     q0=2.0305269096909254
#     q1=4
#     q2=5
#     q3=5.201377505310179
#     q4=-3.5
#     q5=-3.465904348351117
#     q6=-4
#     q7=4
#     filename = 'general_tt43_python_no_foil_quad_shunt.madx'
#     x_err = {
#         0: {'name': 'quad0', 'strength': q0, 'type': 'quadrupole', 'norm': 7, 'limits':(0,1)},
#         1: {'name': 'quad1', 'strength': q1, 'type': 'quadrupole', 'norm': 7, 'limits':(0,1)},
#         2: {'name': 'quad2', 'strength': q2, 'type': 'quadrupole', 'norm': 7, 'limits':(0,1)},
#         3: {'name': 'quad3', 'strength': q3, 'type': 'quadrupole', 'norm': 7, 'limits':(0,1)},
#         4: {'name': 'quad4', 'strength': q4, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,0)},
#         5: {'name': 'quad5', 'strength': q5, 'type': 'quadrupole', 'norm': 7, 'limits':(-1,0)},
#         6: {'name': 'quad6', 'strength': q6, 'type': 'quadrupole', 'norm': 7, 'limits': (-1, 1)},
#         7: {'name': 'quad7', 'strength': q7, 'type': 'quadrupole', 'norm': 7, 'limits': (-1, 1)},}

# errenv = errorEnv.Error(env.x_best, x, init_dist, n_particles, filename=filename)
# nseeds = 100

# if not run_error:
#     sys.exit()
# start_point = 2
# i = start_point
# n = start_point
# # while i<=100:
# while i<3:
# # for i in range(42, 100):
#     # errorEnv.set_beam_offset()
#     # x, y, px, py = errorEnv.set_beam_jitter_avg()
#     # print(x, y, px, py)
#     errenv = errorEnv.Error(env.x_best, x, init_dist, n_particles, filename=filename)
#     # errorEnv.set_jitters()
#     errenv.avg_iters = 1
#     errenv.set_jitters()
#     errenv.shot = 0
#     errenv.seed = n
#     errenv.addError(n)
#     power_jitter = 7e-6
#     # power_jitter_ascaled = power_jitter/np.sqrt(errorEnv.avg_iters)
#     errenv.addPowerConv(power_jitter, n)
#     bs_temp = errenv.track()
#     beam_size_before_x = bs_temp[0]
#     beam_size_before_y = bs_temp[1]
#     errenv.switch_off_higher_order()

#     try:
#         QuadScanCombo1 = np.array([("bpm.0", "mqawd.0", "quad0"),
#                                   ("bpm.1", "mqawd.4", "quad4"),
#                                   ("bpm.2", "mqawd.2", "quad2")])

#         QuadScanCombo2 = np.array([("bpm.4", "mqawd.9", "quad1"),
#                                    ("bpm.4", "mqawd.6", "quad5")])


#         QuadScanCombo3 = np.array([("bpm.6", "mqawd.10", "quad3"),
#                                    ("bpm.8", "mqawd.14", "quad5"),
#                                    ("bpm.8", "mqawd.11", "quad1")])
#         QuadScanCombo = np.vstack((QuadScanCombo1,QuadScanCombo2, QuadScanCombo3))



#         # errorEnv.test(0.5,QuadScanCombo)
#         # QuadScanComboAll = np.vstack((QuadScanCombo1, QuadScanCombo2, QuadScanCombo3)
#         # sys.exit()
#         bpm_x_before1, bpm_x_after1, bpm_y_before1, bpm_y_after1, quad_x_before1, quad_x_after1, \
#             quad_y_before1, quad_y_after1, bpm_pos1,_ = errenv.QuadShunt(0.5, QuadScanCombo1)
#         bpm_x_before2, bpm_x_after2, bpm_y_before2, bpm_y_after2, quad_x_before2, quad_x_after2, \
#             quad_y_before2, quad_y_after2, bpm_pos2,_ = errenv.QuadShunt(0.5, QuadScanCombo2)
#         bpm_x_before3, bpm_x_after3, bpm_y_before3, bpm_y_after3, quad_x_before3, quad_x_after3, \
#             quad_y_before3, quad_y_after3, bpm_pos3,_ = errenv.QuadShunt(0.5, QuadScanCombo3)

#         # for pp in range(2):
#         #     gain = 0.25
#         #     if pp==0:
#         #         bpm_x_before, bpm_x_after, bpm_y_before, bpm_y_after, quad_x_before, quad_x_after, \
#         #         quad_y_before, quad_y_after, bpm_pos,_ = errenv.QuadShunt(0.5, QuadScanCombo1)
#         #     else:
#         #         errenv.QuadShunt(0.7, QuadScanCombo1)
#         # errenv.QuadShunt(1, QuadScanCombo1)
#         # for pp in range(2):
#         #     if pp==0:
#         #         bpm_x_before, bpm_x_after, bpm_y_before, bpm_y_after, quad_x_before, quad_x_after, \
#         #         quad_y_before, quad_y_after, bpm_pos,_ = errenv.QuadShunt(0.5, QuadScanCombo2)
#         #     else:
#         #         errenv.QuadShunt(0.7, QuadScanCombo2)
#         # errenv.QuadShunt(1, QuadScanCombo2)
#         # for pp in range(2):
#         #     if pp==0:
#         #         bpm_x_before, bpm_x_after, bpm_y_before, bpm_y_after, quad_x_before, quad_x_after, \
#         #         quad_y_before, quad_y_after, bpm_pos,_ = errenv.QuadShunt(0.5, QuadScanCombo3)
#         #     else:
#         #         errenv.QuadShunt(0.7, QuadScanCombo3)
#         # errenv.QuadShunt(1, QuadScanCombo3)

#         # errorEnv.QuadShunt(0.3, QuadScanCombo2)
#         # errorEnv.QuadShunt(0.5, QuadScanCombo2)
#         # errorEnv.QuadShunt(0.7, QuadScanCombo2)
    	
#         # errorEnv.QuadShunt(0.3, QuadScanCombo2)
#         # errorEnv.QuadShunt(0.5, QuadScanCombo2)
#         # errorEnv.QuadShunt(0.7, QuadScanCombo2)

#         # errorEnv.QuadShunt(0.3, QuadScanCombo3)
#         # errorEnv.QuadShunt(0.5, QuadScanCombo3)
#         # errorEnv.QuadShunt(0.7, QuadScanCombo3)
#         # errorEnv.filename = 'general_tt43_python_no_foil.madx'
#         # errorEnv.x = x
#         # # bpm_x_before, bpm_x_after, bpm_y_before, bpm_y_after, quad_x_before, quad_x_after, \
#         # # quad_y_before, quad_y_after, bpm_pos = errorEnv.quadshunt_plot(2, 0.7)
        

#         # # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, \
#         # # _, quad_y_after, bpm_pos = errorEnv.quadshunt_plot(1, 0.7)
#         # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, _, quad_y_after, _, corr_x, corr_y = errenv.dfs_plot(1, 3, 0.7, True)
#         # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, _, quad_y_after, _, corr_x, corr_y = errenv.dfs_plot(1, 1, 0.95, True)

#         # errenv.switch_on_higher_order()
#         # # errorEnv.SextOptScan(np.array(
#         # #         ["mqawd.0:1", "mqawd.4:1", "mqawd.2:1", "mqawd.9:1", "mqawd.6:1", "mqawd.10:1", "mqawd.14:1", "mqawd.11:1"]))

        

#         # print('start optimization')
#         # logging.basicConfig(filename="pybq_noise.log", level=logging.INFO,
#         #             format='%(message)s', filemode='w')
#         # methods = ['Nelder-Mead']  
#         # magnets = np.array(["sd3:1", "sd1:1", "sd5:1", "sd2:1", "sd6:1", "sd4:1","oct7:1","oct6:1","oct11:1"])
#         # magnets_to_offset = [any(b in s.lower() for b in magnets) for s in errenv.madx.table["efield"]["name"]]
#         # print(errenv.madx.table["efield"]["name"][magnets_to_offset])
#         # errenv.initial_x_offset = errenv.madx.table["efield"]["dx"][magnets_to_offset]
#         # errenv.initial_y_offset = errenv.madx.table["efield"]["dy"][magnets_to_offset] 

#         # for p in methods:
#         # p = 'Nelder-Mead'
            
#         # errenv.method = p
#         # # if p == methods[0]:
#         # bpm_x_after, bpm_y_after =    errenv.SextOptScan(np.array(
#         # ["sd3:1", "sd1:1", "sd5:1", "sd2:1", "sd6:1", "sd4:1","oct7:1","oct6:1","oct11:1"]))
#         #     init_offx = errenv.initial_x_offset
#         #     init_offy = errenv.initial_y_offset
#         # else:
#         #     errenv.initial_y_offset = init_offy
#         #     errenv.initial_x_offset = init_offx
#         #     errenv.reset_offsets()
#         # bpm_x_after, bpm_y_after =    errenv.SextOptScan(np.array(
#         #     ["sd3:1", "sd1:1", "sd5:1", "sd2:1", "sd6:1", "sd4:1","oct7:1","oct6:1","oct11:1"]))
#         #     print('Finish optimization')
#         # sys.exit()
        
#         # sys.exit()
#         # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, _, quad_y_after, _, corr_x, corr_y = errorEnv.dfs_plot(1, 1, 1, False)
#         # errorEnv.OctOptScan()
#         # Takes number of seeds as input
#         #     # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, \
#         #     #     _, quad_y_after, _ = errorEnv.dfs_plot(i, 1, 2, 0.95)   # Takes number of seeds as input
#         #     # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, \errorEnv.dfs_plot(i, 1, 2, 0.95)
#         #     # _, quad_y_after, _ = errorEnv.dfs_plot(i, 2, 1, 0.7)  # Takes number of seeds as input
#         #     # _, bpm_x_after, _, bpm_y_after, _, quad_x_after, \
#         #     #         _, quad_y_after, bpm_pos = errorEnv.quadshunt_plot(i, 1, 0.95)  # Takes number of seeds as input

#     # except (IndexError, ValueError, RuntimeWarning):
#     except (IndexError):
#         print('Failed to finish optimization! Proceed to next seed')
#         n=n+1
#         continue
#     print('check:1')
#     i = i+1
#     n = n+1


# # Error during operation
# # errorEnv.avg_iters = 1
# # e_offset_x = []
# # e_offset_y = []
# # bs_x = []
# # bs_y = []
# # for shots in range(1000):
# #     # errorEnv.set_beam_jitter()
# #     bs_temp = errorEnv.track()
# #     beam_size_after_x = bs_temp[0]
# #     beam_size_after_y = bs_temp[1]
# #     offset_x = bs_temp[2]
# #     offset_y = bs_temp[3]
# #     e_offset_x.append(offset_x)
# #     e_offset_y.append(offset_y)
# #     bs_x.append(beam_size_after_x)
# #     bs_y.append(beam_size_after_y)

# # p_offset_x = np.random.normal(scale=81.68e-6, size=100)
# # p_offset_y = np.random.normal(scale=10.49e-6, size=100)
# # offsets = {}
# # offsets['electrons_x'] = e_offset_x
# # offsets['electrons_y'] = e_offset_y
# # offsets['protons_x'] = p_offset_x
# # offsets['protons_y'] = p_offset_y
# # offsets['bs_x'] = bs_x
# # offsets['bs_y'] = bs_y


# # filename = 'data/offset_avg_1shots_1000_ne'
# # outfile = open(filename, 'wb')
# # pickle.dump(offsets, outfile)
# # outfile.close()

#     # print('check:3')
#     # ax.plot(bpm_pos, bpm_x_before * 10 ** 3, '-', color=[0, 0.324219, 0.628906], label=None)
#     # ax.plot(bpm_pos, bpm_x_after * 10 ** 3, '-', color="darkorange", label=None)
#     # ax1.plot(bpm_pos, bpm_y_before * 10 ** 3, '-', color=[0, 0.324219, 0.628906], label=None)
#     # ax1.plot(bpm_pos, bpm_y_after * 10 ** 3, '-', color="darkorange", label=None)
# # # Plotting for the error studies
# # #
# #
# # if nseeds > 1:
# #     offsets_before = [bpm_x_before, bpm_y_before]
# #     offsets_after = [bpm_x_after, bpm_y_after]
# #     sizes_before = [beam_size_x_before_all, beam_size_y_before_all]
# #     sizes_after = [beam_size_x_after_all, beam_size_y_after_all]
# #     fig = plt.figure()
# #     gs = matplotlib.gridspec.GridSpec(1, 2, wspace=0.25, hspace=0.7)
# #     ax0 = fig.add_subplot(gs[0])
# #     ax1 = fig.add_subplot(gs[1])
# #     # ax2 = fig.add_subplot(gs[2])
# #     # ax3 = fig.add_subplot(gs[3])
# #     # ax4 = fig.add_subplot(gs[4])
# #     # ax5 = fig.add_subplot(gs[5])
# #     # ax6 = fig.add_subplot(gs[6])
# #     # ax7 = fig.add_subplot(gs[7])
# #     # ax8 = fig.add_subplot(gs[8])
# #     i = 0
# #     ax0.hist(sizes_before[i], alpha=0.8,
# #              label="Mean before = %.2f $\mu$m" % np.multiply(1, np.mean(sizes_before[i])))
# #     ax0.hist(sizes_after[i], alpha=0.8,
# #              label="Mean after = %.2f $\mu$m" % np.multiply(1, np.mean(sizes_after[i])))
# #     ax0.legend()

# #     ax0.set_xlabel('Beam size in $\mu$m')
# #     ax0.set_ylabel('Frequency')
# #     ax0.set_title('Horizontal')
# #     i = 1
# #     ax1.hist(sizes_before[i], alpha=0.8,
# #              label="Mean before = %.2f $\mu$m" % np.multiply(1, np.mean(sizes_before[i])))
# #     ax1.hist(sizes_after[i], alpha=0.8,
# #              label="Mean after = %.2f $\mu$m" % np.multiply(1, np.mean(sizes_after[i])))
# #     ax1.legend()

# #     ax1.set_xlabel('Beam size in $\mu$m')
# #     ax1.set_ylabel('Frequency')
# #     ax1.set_title('Vertical')
# #     # fig = plt.figure()
# #     # for idx2, ax in enumerate([ax0, ax1]):
# #     #     ax = fig.add_subplot(gs[idx2])
# #     #     ax.hist(1000000 * offsets_before[i][idx2], alpha=0.8,
# #     #             label="Offsets before = %.2f $\mu$m" % np.multiply(1, np.std(
# #     #                 1000000 * offsets_before[i][idx2])))
# #     #     ax.hist(1000000 * offsets_after[i][:idx2], alpha=0.8,
# #     #             label="Offsets after = %.2f $\mu$m" % np.multiply(1, np.std(
# #     #                 1000000 * offsets_after[i][idx2])))
# #     #     ax.legend()
# #     #     ax.set_xlabel('Offset in $\mu$m')
# #     #     ax.set_ylabel('Frequency')
# #     #     ax.set_title('Quad = %s' % idx2)
# #     # plt.show()
# #     fig = plt.figure()
# #     gs = matplotlib.gridspec.GridSpec(1, 2, wspace=0.25, hspace=0.7)
# #     ax0 = fig.add_subplot(gs[0])
# #     ax1 = fig.add_subplot(gs[1])
# #     # ax2 = fig.add_subplot(gs[2])
# #     # ax3 = fig.add_subplot(gs[3])
# #     # ax4 = fig.add_subplot(gs[4])
# #     # ax5 = fig.add_subplot(gs[5])
# #     # ax6 = fig.add_subplot(gs[6])
# #     # ax7 = fig.add_subplot(gs[7])
# #     # ax8 = fig.add_subplot(gs[8])
# #     i = 0
# #     ax0.hist(offsets_before[i], alpha=0.8,
# #              label="Jitter before = %.2f $\mu$m" % np.multiply(1, np.std(offsets_before[i])))
# #     ax0.hist(offsets_after[i], alpha=0.8,
# #              label="Jitter after = %.2f $\mu$m" % np.multiply(1, np.std(offsets_after[i])))
# #     ax0.legend()

# #     ax0.set_xlabel('Beam jitter at injection-point in $\mu$m')
# #     ax0.set_ylabel('Frequency')
# #     ax0.set_title('Horizontal')
# #     i = 1
# #     ax1.hist(offsets_before[i], alpha=0.8,
# #              label="Jitter before = %.2f $\mu$m" % np.multiply(1, np.std(offsets_before[i])))
# #     ax1.hist(offsets_after[i], alpha=0.8,
# #              label="Jitter after = %.2f $\mu$m" % np.multiply(1, np.std(offsets_after[i])))
# #     ax1.legend()

# #     ax1.set_xlabel('Beam jitter at injection-point in $\mu$m')
# #     ax1.set_ylabel('Frequency')
# #     ax1.set_title('Vertical')
